![MediaPipe](docs/images/mediapipe_small.png)

--------------------------------------------------------------------------------

## Live ML anywhere

[MediaPipe](https://google.github.io/mediapipe/) offers cross-platform, customizable
ML solutions for live and streaming media.

This version of MediaPipe is being developed for the 4th semester project at the [Institute for Man-Machine-Interaction](https://www.mmi.rwth-aachen.de/) (MMI) at [RWTH Aachen University](www.rwth-aachen.de) in the summer semester of 2022.

--------------------------------------------------------------------------------

This is a custom, purpose-built version of MediaPipe that:
- Adds a named pipe at `\\\\.\\pipe\\handDetection` that extracts all landmarks of the detected hand. This pipe can be picked up from an external process to use in your program.
    - Note that named pipes are Windows exclusive, so that is currently the only supported system. I am planning to implement a UNIX compatible version in the future.
- Removes unneeded clutter from the codebase that the Hands example doesn't use. This means that other examples **cannot** be used with this fork.

Also, to run this properly, it's important that the *receiving* end of the Pipe (i.e. not this one) is started before running MediaPipe.

### Prerequisites
The same as the regular MediaPipe repository, plus a Windows system. Follow the [MediaPipe installation guide](https://google.github.io/mediapipe/getting_started/install.html#installing-on-windows) for a detailed guide.
Also, opencv needs to be build with opencv_contrib and the contents of the `build/install` folder needs to be copied to `C:\\opencv\build\`.

Make sure to update `C:\\Users\<name>\_bazel_<name>\<random string>\external\windows_opencv\BUILD.bazel` to represent the right paths in the `opencv\build` folder.

### Building
My build command is as follows
```ps
bazel build -c opt --define MEDIAPIPE_DISABLE_GPU=1 --action_env PYTHON_BIN_PATH="C://Users//[your username]//AppData//Local//Programs//Python//Python310//python.exe" mediapipe/examples/desktop/hand_tracking:hand_tracking_cpu
```
For you it *should* be similar, though your Python path may be slightly different.

### Running
My run command is as follows
```ps
.\bazel-bin\mediapipe\examples\desktop\hand_tracking\hand_tracking_cpu.exe --calculator_graph_config_file .\mediapipe\graphs\hand_tracking\hand_tracking_desktop_live.pbtxt
```
Again, this should be the same for you.

### Receive Program
The following program can be used to receive the data. Customize to your liking. [Taken from this StackOverflow Answer](https://stackoverflow.com/a/26561999/12151348).
```cpp
#include <windows.h> 
#include <stdio.h> 
#include <tchar.h>
#include <strsafe.h>

#define BUFSIZE 512

int main(void)
{
    HANDLE hPipe;
    char buffer[2024];
    DWORD dwRead;


    hPipe = CreateNamedPipe(TEXT("\\\\.\\pipe\\handDetection"),
        PIPE_ACCESS_DUPLEX,
        PIPE_TYPE_BYTE | PIPE_READMODE_BYTE | PIPE_WAIT,   // FILE_FLAG_FIRST_PIPE_INSTANCE is not needed but forces CreateNamedPipe(..) to fail if the pipe already exists...
        1,
        2024 * 16,
        2024 * 16,
        NMPWAIT_USE_DEFAULT_WAIT,
        NULL);

    if (hPipe == INVALID_HANDLE_VALUE) {
        printf("INVALID_HANDLE_VALUE\n");
    }

    while (hPipe != INVALID_HANDLE_VALUE)
    {
        if (ConnectNamedPipe(hPipe, NULL) != FALSE)   // wait for someone to connect to the pipe
        {
            while (ReadFile(hPipe, buffer, sizeof(buffer) - 1, &dwRead, NULL) != FALSE)
            {
                /* add terminating zero */
                buffer[dwRead] = '\0';

                /* do something with data in buffer */
                printf("%s", buffer);
            }
        }

        //DisconnectNamedPipe(hPipe);
    }

    return 0;
}
```